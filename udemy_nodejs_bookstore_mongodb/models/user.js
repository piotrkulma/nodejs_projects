const ObjectId = require('mongodb').ObjectId
const getDb = require('../util/database').getDb

class User {
    constructor(userName, email) {
        this.userName = userName
        this.email = email
    }

    save() {
        return getDb()
            .collection('users')
            .insertOne(this)
            .then(result => {
                return result
            })
            .catch(err => {
                console.log(err)
            })
    }

    static findById(userId) {
        return getDb()
            .collection('users')
            .findOne({_id: new ObjectId(userId)})
            .then(result => {
                console.log(result)
                return result
            })
            .catch(err => {
                console.log(err)
            })
    }
}

module.exports = User