const fetchData = () => {
    const promise = new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve({name: 'xxx', id: 3333})
        }, 1500)
    })

    return promise
}

fetchData().then(data => {
    console.log(data)
})

console.log('xxx')