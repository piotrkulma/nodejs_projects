const express = require('express')

const router = express.Router();
const users = require('./insert').users

router.get('/users', (req, res) => {
    res.render('users', {users: users})
})

router.get('/api/users', (req, res) => {
    res.send(users)
})

module.exports = {
    usersRouter: router
}