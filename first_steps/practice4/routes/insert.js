const express = require('express')
const router = express.Router()
const users = [{name: 'Jan'}]

router.get('/', (req, res) => {
    res.render('insertUser')
})

router.post('/', (req, res) => {
    users.push({name: req.body.name})
    res.redirect('/users')
})

module.exports= {
    userRouter: router,
    users: users
}