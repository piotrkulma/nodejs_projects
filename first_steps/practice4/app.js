const express = require('express')
const insertUserRouter = require('./routes/insert')
const usersRouter = require('./routes/users')
const bodyParser = require('body-parser')
const path = require('path')

const app = express()

app.set('view engine', 'ejs')
app.set('views', 'views')

app.use(express.static(path.join(__dirname, 'public')))

app.use(bodyParser.urlencoded())

app.use(insertUserRouter.userRouter)
app.use(usersRouter.usersRouter);

app.listen(3000)