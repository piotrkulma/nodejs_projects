const express = require('express')

const app = express()

app.use("/users", (req, res, next) => {
    console.log("mid 2")
    res.send([{id: 1, name: 'Peter'}, {id: 2, name: 'John'}])
})

app.use("/", (req, res, next) => {
    console.log("mid 1")
    res.send('<h1>response /</h1>')
})

app.listen(3000)