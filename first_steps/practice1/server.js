const http = require('http')
const fs = require('fs')

let nextUserId = 3
const users = [
    {id: 1, name: 'Jan Nowak'},
    {id: 2, name: 'Marian Kowalski'}
]

const server = http.createServer((req, res) => {
    const url = req.url
    const method = req.method

    if (url === '/users' && method === 'GET') {
        res.setHeader('Content-Type', 'application/json')
        res.write(JSON.stringify(users))
        return res.end()
    } else if (url === '/create-user' && method === 'POST') {
        const data = []

        req.on('data', (chunk) => {
            data.push(chunk)
        })

        return req.on('end', () => {
            const parsedData = Buffer.concat(data).toString().split("=")
            users.push({id: nextUserId++, name: parsedData[1]})
            fs.writeFile('user.txt', parsedData[1], (err) => {
                res.statusCode = 302
                res.setHeader('Location', '/')
                return res.end()
            })
        })
    }

    res.setHeader('Content-Type', 'text/html')
    res.write(`
        <html>
            <body>
                <b>Greetings</b><br/>
                <form action="/create-user" method="POST">
                    <input type="text" name="name"/><br/>
                    <button type="submit">Submit</button>
                </form>
            </body>
        </html>
        `)
    res.end()
})

server.listen(3000)